<!DOCTYPE html>
<html lang="en-US" dir="ltr">
    <head>
        <title>Structuring Tabular Data</title>
        <meta charset="UTF-8">
        <link rel="icon" type="image/x-icon" href="imags/favicon.ico">
</head>
<table border="1" align="center" width="480px">
    <caption>Shop list for 25th of May</caption>
    <colgroup>
        <col>
        <col>
        <col>
    </colgroup>
    <thead>
        <tr>
            <th scope="col" rowspan="2">Name</th>
            <th scope="col" colspan="3">Purchases</th>
        </tr>
        <tr>
            <th scope="col">Price</th>
            <th scope="col">Amount (kilo)</th>
            <th scope="col">Sum</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td align="left">Apple</td>
            <td>20</td>
            <td>2</td>
            <td>40</td>
        </tr>
        <tr>
            <td align="left">Potato</td>
            <td>10</td>
            <td>3</td>
            <td>30</td>
        </tr>
        <tr>
            <td align="left">
                Carrot
            </td>
            <td>15</td>
            <td>1</td>
            <td>15</td>
        </tr>

    </tbody>
    <tfoot>
        <tr>
            <th colspan="2" align="left">Total</th>
            <td>3</td>
            <td>85</td>
        </tr>
    </tfoot>
</table>